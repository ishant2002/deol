import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit{
  contactform!:FormGroup;
  iscontactForm:boolean=false;
  
  
  constructor(public route:Router,public fb: FormBuilder ) {
  }
  ngOnInit() {
    this.initcontactForm();
  }
  initcontactForm() {
    this.contactform = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      phone: ['', [Validators.required, Validators.pattern('[0-9]*')]], 
     message:['',Validators.required]
    });
  }
   
  
  onSubmit() {
    this.iscontactForm=true;
    if (this.contactform.valid) {
      let formData = this.contactform.value;
      sessionStorage.setItem('contactFormData', JSON.stringify(formData));
    } else {
    }
  }
}

