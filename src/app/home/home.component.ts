import { Component, OnInit } from '@angular/core';
import { FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { LocationService } from '../services/location.service';
import { VehicleService } from '../services/vehicle.service';
import { environment } from 'src/environment/environment';
import { Route, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AddonbehaviourService } from '../services/addonbehaviour.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  image: { url: string, alt: string }[] = [
    { url: "/assets/automation.avif", alt: "slider 1" },
    { url: "/assets/inauto.webp", alt: "slider 2" },
    { url: "/assets/tubesauto.avif", alt: "slider 3" },
    { url: "/assets/sorting.jpeg", alt: "slider 4" },
  ];
  appVersion: Date = new Date();
  currentSlideIndex: number = 0; // Initialize here
  constructor(public route: Router) { }

  ngOnInit() {
    setInterval(() => {
      this.nextSlide();
    }, 5000);
  }

  nextSlide() {
    this.currentSlideIndex = (this.currentSlideIndex + 1) % this.image.length;
  }
}








//   customerDetails:any = [];
//   currentSlideIndex: number = 0;
//   location: Array<any> = [];
//   homeLocation:any = [];
//   data: any;
//   locationList: any;
//   vehicleName: any;
//   searchForm: any;
//   imagePath = environment._imgUri;
//   isSubmitForm: boolean = false;
//   toastrService: any;
//   vehicleId: any;
// vehicle:any;
//   image: { url: string, alt: string }[] = [
//     { url: "/assets/car_1.png", alt: "slider 1" },
//     { url: "/assets/car-2.png", alt: "slider 2" },
//     { url: "/assets/car_3.png", alt: "slider 3" },
//     { url: "/assets/truck 1.png", alt: "slider 4" },
//     { url: "/assets/truck-2.png", alt: "slider 5" }
//   ];

//   constructor(public locationService: LocationService,public  addonbehaviourService: AddonbehaviourService, private toastr: ToastrService, public vehicleService: VehicleService, public route: Router) { }
//   ngOnInit() {
//     this.vehicleId = sessionStorage.getItem("selectedVehicleId");
//     setInterval(() => {
//       this.nextSlide();
//     }, 5000);
//     this.initSearch();
//      let searchData = sessionStorage.getItem('searchform');
//     if (searchData) {
//       const data = JSON.parse(searchData);
//       this.searchForm.patchValue({
//         pickuplocation: data?.pickuplocation,
//         dropdownlocation: data?.dropdownlocation,
//         datepicker: data?.datepicker,
//         pickupdate: data?.pickupdate,
//         all: data?.all,
//       });
//     }
  

//     this.loadCustomerDetails();
//     this.homePageLocation();
//     this.vehicleLocation();
//   }

//   nextSlide() {
//     this.currentSlideIndex = (this.currentSlideIndex + 1) % this.image.length;
//   }

//   loadCustomerDetails() {
//     this.customerDetails = [
//       { img: "/assets/Group 41.png", heading: "Australia Owned Business", about: "Residing and running our owned business in Australia for more than a decade, our quality fits into standards that our clients look for" },
//       { img: "/assets/Group 41 (1).png", heading: "Quality Vehicles", about: "Residing and running our owned business in Australia for more than a decade, our quality fits into standards that our clients look for" },
//       { img: "/assets/Group 41 (2).png", heading: "Customised Solution", about: "Residing and running our owned business in Australia for more than a decade, our quality fits into standards that our clients look for" },
//       { img: "/assets/Group 41 (3).png", heading: "Direct Contact With Agent", about: "Residing and running our owned business in Australia for more than a decade, our quality fits into standards that our clients look for" },
//       { img: "/assets/Group 41 (6).png", heading: "Best Prices Guarantee", about: "Residing and running our owned business in Australia for more than a decade, our quality fits into standards that our clients look for" },
//       { img: "/assets/Group 41 (5).png", heading: "Short & Long Term Available", about: "Residing and running our owned business in Australia for more than a decade, our quality fits into standards that our clients look for" }
//     ];
//   }

//   initSearch() {
//     this.searchForm = new FormGroup({
//       pickuplocation: new FormControl('', Validators.required),
//       dropdownlocation: new FormControl('', Validators.required),
//       all: new FormControl('all', Validators.required),
//       datepicker: new FormControl('', Validators.required),
//       pickupdate: new FormControl('', Validators.required),
//     });
//   }

//   onClickSearch() {
//     this.isSubmitForm = true;
//     if (this.searchForm.valid) {
//       sessionStorage.setItem('searchform', JSON.stringify(this.searchForm.value));
//       sessionStorage.setItem('selectedVehicleId',this.searchForm.value.all);
//       // sessionStorage.setItem('selectedVehicle', JSON.stringify(this.searchForm.value));
//       this.route.navigate(['/addons']); 
//     } else {
//       this.toastr.error('Please fill the data!', 'Error');
//     }
//   }

//   dateToday(): Date {
//     const now = new Date();
//     const year = now.getFullYear();
//     const month = now.getMonth();
//     const day = now.getDate();
//     const hour = now.getHours();
//     const minute = now.getMinutes();
//     return new Date(year, month, day, hour, minute);
//   }

//   homePageLocation() {
//     this.locationService.getLocation().subscribe(res => {
//       this.locationList = res.response.data.allLocations;
//       this.location = res.response.data.homeLocations;
//     });
//   }

//   vehicleLocation() {
//     this.vehicleService.getVehicle().subscribe(res => {
//       this.vehicleName = res.response.data;
//       this.vehicleId = res?.response?.data?.[0]?._id;
      
//     });
//   }
 
//   onLocation(id: any) {
//     this.route.navigate(['/locationdetails', id]);
//   }
//   onCompany(){
//   }





 // vehicleLocation() {
  //   this.vehicleService.getVehicle().subscribe(res => {
  //     this.vehicleName = res.response.data;
  //     const selectedVehicle = this.vehicleName?.[0];
  //     if (selectedVehicle) {
  //       this.vehicleService.setVehicle(selectedVehicle);
  //     }
  //   });
  // }

