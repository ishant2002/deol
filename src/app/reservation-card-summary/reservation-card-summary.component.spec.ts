import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationCardSummaryComponent } from './reservation-card-summary.component';

describe('ReservationCardSummaryComponent', () => {
  let component: ReservationCardSummaryComponent;
  let fixture: ComponentFixture<ReservationCardSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservationCardSummaryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReservationCardSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
