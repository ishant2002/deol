import { Component, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { LocationService } from '../services/location.service';
import { VehicleService } from '../services/vehicle.service';
import { AddonsService } from '../services/addons.service';
import { SelectedvehicleService } from '../services/selectedvehicle.service';
import { AddonbehaviourService } from '../services/addonbehaviour.service';

@Component({
  selector: 'app-reservation-card-summary',
  templateUrl: './reservation-card-summary.component.html',
  styleUrls: ['./reservation-card-summary.component.css']
})
export class ReservationCardSummaryComponent {
  locations: any;
  dropLocation: any;
  vehicles: any;
  searchDataAvailable: any;
  addOnList: any;
  addList: any;
  selectedAddOnsdata: any;
  addondata: any;
  vehicleCategoryId: any;
  isRouteActive(route: string): boolean {
    return this.route.isActive(route, true);
  }
  locationsearchList: any;
  vehiclesearchlist:any;
  dropLocationId:any;
  pickupLocationId:any;
  pickupDate:any;
  dropdate:any;
  vehicleName:any
  vehicleId:any;
  pickupLocation:any;
  dropdownlocation:any;
  addOns:any;
  neccessaryAddOnsList:any;
  vehicleadd:any;
  vehicle:any;
  dropdownLocation:any;
  selectedVehicle: any;
  selectedAddOns: any[] = [];
  constructor(public route:Router,public locationService:LocationService,public vehicleService:VehicleService,public addonsService:AddonsService,public selectedvehicleService : SelectedvehicleService,){ 
  }
  ngOnInit() {
    let searchData = sessionStorage.getItem('searchform')
    let data = JSON.parse(searchData || '')
    if (data) {
      this.pickupLocationId=data.pickuplocation;
    this.dropLocationId=data.dropdownlocation;
    this.dropdate=data.pickupdate;
    this.pickupDate=data.datepicker;
    this.vehicleId=data.all;
    }
    this.searchLocationData(); 
    this.getAddOnList();
    this.getVehicleId();

  }
  searchLocationData() {
    this.locationService.getLocation().subscribe(res => {
      if (res.response.success == true) {
        this.locations = res.response?.data?.allLocations;
        this.pickupLocation = this.locations.find((x: any) => x.id === this.pickupLocationId)
        this.dropLocation  = this.locations.find((x: any) => x.id === this.dropLocationId)
      }
    
    });
  
  }
    getVehicleId(){
    this.addonsService.getidVehicle(this.vehicleId)
    .subscribe(
      (res: any) => {
        console.log("vehicel",this.vehicleId)
      this.selectedAddOnsdata=res?.response?.data;
      console.log("selectedAddOnsdata",this.selectedAddOnsdata)
      sessionStorage.setItem('vehicleDays', this.selectedAddOnsdata.pricePerDay );
      this.addondata=res.response.data.addOn;
      console.log("addon")
      this.vehicleCategoryId=res.response.data._id;
       },
     )
  }
  // getVehicle(){
  //   this.vehicleService.getVehicle().subscribe(res => {
  //     if (res.response.success == true) {
  //       //called in ngoninit
  //       this. vehicles = res.response.data;
  //       this.vehicleName = this.vehicles.find((x: any) => x._id ===  this.vehicleId);
  //       console.log(this.vehicleName);
  //     }
  //   }, (error: any) => {
  //     console.log(error)
  //   })    
  // }
  getAddOnList() {
    this.addonsService.getAddOns().subscribe(res => {
      if (res.response.success == true) {
        // console.log("addonname.....", res);
        this.addOnList = res.response?.data;
        this.addList = this.addOnList.filter(
          (x: any) => x.addonType === 1,

        )
        // console.log(this.addList);
      }
    }, (error: any) => {
      // console.log(error)
    })
  }
}


  // const locations = res.response.data.allLocations;
      // const pickupLocation = locations.find((location:any) => location._id === this.pickupLocationId);
      // this.pickupLocation = pickupLocation?.name;
      // const dropLocation = locations.find((location:any) => location._id === this.dropLocationId);
      // this.dropdownLocation = dropLocation?.name;



  // getAddOnList(){
  //   this.addonsService.getAddOns().subscribe(res=>{
  //     this.neccessaryAddOnsList=res.response.data.filter((x:any) => x.addonType === 1);
  //   })
  // }
  


    // const selectedAddOnsData = sessionStorage.getItem('selectedAddOns');
    // if (selectedAddOnsData) {
    //   this.selectedAddOns = JSON.parse(selectedAddOnsData);
    // }
    // this.selectedvehicleService.reservationData$.subscribe(updatedData => {
    //   if (updatedData) {
    //     this.pickupLocationId = updatedData.pickuplocation;
    //     this.dropLocationId = updatedData.dropdownlocation;
    //     this.dropdate = updatedData.pickupdate;
    //     this.pickupDate = updatedData.datepicker;
    //     this.searchLocationData();
    //   }
    // });
    // this.addonbehaviourService.selectedVehicle$.subscribe((vehicle:any) => {
    //   if (vehicle) {
    //     this.selectedVehicle = vehicle;
    //     this.vehicleName = vehicle.categoryName;
    //   }
    // });



    // this.selectedvehicleService.reservationData$.subscribe(data => {
    //   if (data) {
    //     this.pickupLocationId = data.pickuplocation;
    //     this.dropLocationId = data.dropdownlocation;
    //     this.pickupDate = data.datepicker;
    //     this.dropDate = data.pickupdate;
    //     this.vehicleId = data.vehicleId;
    //     this.searchLocationData();
    //   }
    // });

    // const selectedAddOnsData = sessionStorage.getItem('selectedAddOns');
    // if (selectedAddOnsData) {
    //   this.selectedAddOns = JSON.parse(selectedAddOnsData);
    // }


  // searchLocationData() {
  //   this.locationService.getLocation().subscribe(res => {
  //     const locations = res.response.data.allLocations;
  //     const pickupLocation = locations.find((location:any) => location._id === this.pickupLocationId);
  //     this.pickupLocation = pickupLocation?.name;
  //     const dropLocation = locations.find((location:any) => location._id === this.dropLocationId);
  //     this.dropdownLocation = dropLocation?.name;
  //   });

  //   this.vehicleService.getVehicle().subscribe(res => {
  //     const vehicles = res.response.data;
  //     const vehicle = vehicles.find((vehicle:any) => vehicle._id === this.vehicleId);
  //     this.vehicleName = vehicle?.categoryName;
  //   });
  // }