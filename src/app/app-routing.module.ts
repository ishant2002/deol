import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { ServiceComponent } from './service/service.component';


const routes: Routes = [
  {
    path: "",
    component:HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
 {
  path:"about",
  component:AboutusComponent,
 },
 {
  path:"contact",
  component:ContactusComponent,
 },
 {
  path:'service',
  component:ServiceComponent,
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
