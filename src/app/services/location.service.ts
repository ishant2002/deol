import { Injectable } from '@angular/core';
import { HttpServices } from './httpService';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  constructor( public httpService:HttpServices) { }
  getLocation(){
    return this.httpService.get('website/homedata')
  }
  getlocationDetails(id:any){
    return this.httpService.get('website/location/'+id)   
  }
}
