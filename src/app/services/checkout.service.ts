import { Injectable } from '@angular/core';
import { HttpServices } from './httpService';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(public httpService:HttpServices) { }
  getCheckout(couponCode:any){
    return this.httpService.get('website/coupon/'+couponCode)
  }
  
}
