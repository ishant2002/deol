import { Injectable } from '@angular/core';
import { HttpServices } from './httpService';


@Injectable({
  providedIn: 'root'
})
export class AddonsService {
  httpClient: any;
  baseUrl: any;
 
  constructor(public httpService:HttpServices) { }
  getAddOns(){
    return this.httpService.get( 'website/addons/list')
  }

  addOnCalculation( vehicleId: any, datepicker: any, 
    pickupdate: any, selectedAddOns: any) {
    const body= {
      vehicleCategoryId: vehicleId,
      pickupDate:datepicker,
      dropDate:pickupdate,
      addonData: selectedAddOns,
    };
return this.httpService.post('website/booking/fareCalculation',body)
}
getidVehicle(id:any){
  console.log("id",id)
  return this.httpService.get("website/vehicle",id )
}
}
// postRequest(url: any, data: any) {
//   return this.httpClient.post(this.baseUrl + url, data)
// }

