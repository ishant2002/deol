import { Injectable } from '@angular/core';
import { HttpServices } from './httpService';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  
  constructor(public httpService:HttpServices ) { }
  
  getVehicle(){
    return this.httpService.get("website/vehiclecategories")
  }
}
