import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { HttpServices } from '../services/httpService';
import { LocationService } from '../services/location.service';
import { environment } from 'src/environment/environment';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(public route: Router, public modelService: NgbModal) { }
  isRouteActive(route: string): boolean {
    return this.route.isActive(route, true);
  }
  
}
