import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-popup-locationmap',
  templateUrl: './popup-locationmap.component.html',
  styleUrls: ['./popup-locationmap.component.css']
})
export class PopupLocationmapComponent {
 content:any;
  constructor(public activeModelService: NgbActiveModal){}
 
}
