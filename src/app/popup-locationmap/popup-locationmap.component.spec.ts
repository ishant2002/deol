import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupLocationmapComponent } from './popup-locationmap.component';

describe('PopupLocationmapComponent', () => {
  let component: PopupLocationmapComponent;
  let fixture: ComponentFixture<PopupLocationmapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupLocationmapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PopupLocationmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
